import { RouterView } from 'vue-router';
import MainAppBar from './components/MainAppBar.vue';
import { __VLS_internalComponent, __VLS_componentsOption, __VLS_name, drawer, rail } from './App.vue';

function __VLS_template() {
let __VLS_ctx!: InstanceType<__VLS_PickNotAny<typeof __VLS_internalComponent, new () => {}>> & {};
/* Components */
let __VLS_otherComponents!: NonNullable<typeof __VLS_internalComponent extends { components: infer C; } ? C : {}> & typeof __VLS_componentsOption;
let __VLS_own!: __VLS_SelfComponent<typeof __VLS_name, typeof __VLS_internalComponent & (new () => { $slots: typeof __VLS_slots; })>;
let __VLS_localComponents!: typeof __VLS_otherComponents & Omit<typeof __VLS_own, keyof typeof __VLS_otherComponents>;
let __VLS_components!: typeof __VLS_localComponents & __VLS_GlobalComponents & typeof __VLS_ctx;
/* Style Scoped */
type __VLS_StyleScopedClasses = {};
let __VLS_styleScopedClasses!: __VLS_StyleScopedClasses | keyof __VLS_StyleScopedClasses | (keyof __VLS_StyleScopedClasses)[];
/* CSS variable injection */
/* CSS variable injection end */
let __VLS_resolvedLocalAndGlobalComponents!: {} &
__VLS_WithComponent<'VLayout', typeof __VLS_localComponents, "VLayout", "vLayout", "v-layout"> &
__VLS_WithComponent<'MainAppBar', typeof __VLS_localComponents, "MainAppBar", "mainAppBar", "main-app-bar"> &
__VLS_WithComponent<'VListItem', typeof __VLS_localComponents, "VListItem", "vListItem", "v-list-item"> &
__VLS_WithComponent<'VBtn', typeof __VLS_localComponents, "VBtn", "vBtn", "v-btn"> &
__VLS_WithComponent<'VDivider', typeof __VLS_localComponents, "VDivider", "vDivider", "v-divider"> &
__VLS_WithComponent<'VList', typeof __VLS_localComponents, "VList", "vList", "v-list"> &
__VLS_WithComponent<'VMain', typeof __VLS_localComponents, "VMain", "vMain", "v-main"> &
__VLS_WithComponent<'RouterView', typeof __VLS_localComponents, "RouterView", "routerView", "router-view">;
__VLS_components.VLayout; __VLS_components.VLayout; __VLS_components.vLayout; __VLS_components.vLayout; __VLS_components["v-layout"]; __VLS_components["v-layout"];
// @ts-ignore
[VLayout, VLayout,];
__VLS_components.MainAppBar; __VLS_components.mainAppBar; __VLS_components["main-app-bar"];
// @ts-ignore
[MainAppBar,];
__VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.VListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components.vListItem; __VLS_components["v-list-item"]; __VLS_components["v-list-item"]; __VLS_components["v-list-item"]; __VLS_components["v-list-item"]; __VLS_components["v-list-item"]; __VLS_components["v-list-item"]; __VLS_components["v-list-item"]; __VLS_components["v-list-item"];
// @ts-ignore
[VListItem, VListItem, VListItem, VListItem, VListItem, VListItem, VListItem, VListItem,];
__VLS_intrinsicElements.template; __VLS_intrinsicElements.template;
__VLS_components.VBtn; __VLS_components.VBtn; __VLS_components.vBtn; __VLS_components.vBtn; __VLS_components["v-btn"]; __VLS_components["v-btn"];
// @ts-ignore
[VBtn, VBtn,];
__VLS_components.VDivider; __VLS_components.VDivider; __VLS_components.vDivider; __VLS_components.vDivider; __VLS_components["v-divider"]; __VLS_components["v-divider"];
// @ts-ignore
[VDivider, VDivider,];
__VLS_components.VList; __VLS_components.VList; __VLS_components.vList; __VLS_components.vList; __VLS_components["v-list"]; __VLS_components["v-list"];
// @ts-ignore
[VList, VList,];
__VLS_components.VMain; __VLS_components.VMain; __VLS_components.vMain; __VLS_components.vMain; __VLS_components["v-main"]; __VLS_components["v-main"];
// @ts-ignore
[VMain, VMain,];
__VLS_components.RouterView; __VLS_components.RouterView; __VLS_components.routerView; __VLS_components.routerView; __VLS_components["router-view"]; __VLS_components["router-view"];
// @ts-ignore
[RouterView, RouterView,];
{
const __VLS_0 = ({} as 'VLayout' extends keyof typeof __VLS_ctx ? { 'VLayout': typeof __VLS_ctx.VLayout; } : 'vLayout' extends keyof typeof __VLS_ctx ? { 'VLayout': typeof __VLS_ctx.vLayout; } : 'v-layout' extends keyof typeof __VLS_ctx ? { 'VLayout': (typeof __VLS_ctx)["v-layout"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VLayout;
const __VLS_1 = __VLS_asFunctionalComponent(__VLS_0, new __VLS_0({ ...{}, class: ("rounded rounded-md"), }));
({} as { VLayout: typeof __VLS_0; }).VLayout;
({} as { VLayout: typeof __VLS_0; }).VLayout;
const __VLS_2 = __VLS_1({ ...{}, class: ("rounded rounded-md"), }, ...__VLS_functionalComponentArgsRest(__VLS_1));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_0, typeof __VLS_2> & Record<string, unknown>) => void)({ ...{}, class: ("rounded rounded-md"), });
const __VLS_3 = __VLS_pickFunctionalComponentCtx(__VLS_0, __VLS_2)!;
let __VLS_4!: __VLS_NormalizeEmits<typeof __VLS_3.emit>;
{
const __VLS_5 = ({} as 'MainAppBar' extends keyof typeof __VLS_ctx ? { 'MainAppBar': typeof __VLS_ctx.MainAppBar; } : 'mainAppBar' extends keyof typeof __VLS_ctx ? { 'MainAppBar': typeof __VLS_ctx.mainAppBar; } : 'main-app-bar' extends keyof typeof __VLS_ctx ? { 'MainAppBar': (typeof __VLS_ctx)["main-app-bar"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).MainAppBar;
const __VLS_6 = __VLS_asFunctionalComponent(__VLS_5, new __VLS_5({ ...{ onClick: {} as any, }, "<vNavigationDrawer": (true), modelValue: ((__VLS_ctx.drawer)), rail: ((__VLS_ctx.rail)), permanent: (true), }));
({} as { MainAppBar: typeof __VLS_5; }).MainAppBar;
const __VLS_7 = __VLS_6({ ...{ onClick: {} as any, }, "<vNavigationDrawer": (true), modelValue: ((__VLS_ctx.drawer)), rail: ((__VLS_ctx.rail)), permanent: (true), }, ...__VLS_functionalComponentArgsRest(__VLS_6));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_5, typeof __VLS_7> & Record<string, unknown>) => void)({ ...{ onClick: {} as any, }, "<vNavigationDrawer": (true), modelValue: ((__VLS_ctx.drawer)), rail: ((__VLS_ctx.rail)), permanent: (true), });
const __VLS_8 = __VLS_pickFunctionalComponentCtx(__VLS_5, __VLS_7)!;
let __VLS_9!: __VLS_NormalizeEmits<typeof __VLS_8.emit>;
let __VLS_10 = { 'click': __VLS_pickEvent(__VLS_9['click'], ({} as __VLS_FunctionalComponentProps<typeof __VLS_6, typeof __VLS_7>).onClick) };
__VLS_10 = {
click: $event => {
__VLS_ctx.rail = false;
// @ts-ignore
[drawer, rail, drawer, rail, drawer, rail, rail,];
}
};
{
const __VLS_11 = ({} as 'VListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.VListItem; } : 'vListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.vListItem; } : 'v-list-item' extends keyof typeof __VLS_ctx ? { 'VListItem': (typeof __VLS_ctx)["v-list-item"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VListItem;
const __VLS_12 = __VLS_asFunctionalComponent(__VLS_11, new __VLS_11({ ...{}, prependAvatar: ("https://randomuser.me/api/portraits/men/85.jpg"), title: ("John Leider"), nav: (true), }));
({} as { VListItem: typeof __VLS_11; }).VListItem;
({} as { VListItem: typeof __VLS_11; }).VListItem;
const __VLS_13 = __VLS_12({ ...{}, prependAvatar: ("https://randomuser.me/api/portraits/men/85.jpg"), title: ("John Leider"), nav: (true), }, ...__VLS_functionalComponentArgsRest(__VLS_12));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_11, typeof __VLS_13> & Record<string, unknown>) => void)({ ...{}, prependAvatar: ("https://randomuser.me/api/portraits/men/85.jpg"), title: ("John Leider"), nav: (true), });
const __VLS_14 = __VLS_pickFunctionalComponentCtx(__VLS_11, __VLS_13)!;
let __VLS_15!: __VLS_NormalizeEmits<typeof __VLS_14.emit>;
{
const __VLS_16 = __VLS_intrinsicElements["template"];
const __VLS_17 = __VLS_elementAsFunctionalComponent(__VLS_16);
const __VLS_18 = __VLS_17({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_17));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_16, typeof __VLS_18> & Record<string, unknown>) => void)({ ...{}, });
{
(__VLS_14.slots!).append;
{
const __VLS_19 = ({} as 'VBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.VBtn; } : 'vBtn' extends keyof typeof __VLS_ctx ? { 'VBtn': typeof __VLS_ctx.vBtn; } : 'v-btn' extends keyof typeof __VLS_ctx ? { 'VBtn': (typeof __VLS_ctx)["v-btn"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VBtn;
const __VLS_20 = __VLS_asFunctionalComponent(__VLS_19, new __VLS_19({ ...{ onClick: {} as any, }, variant: ("text"), icon: ("mdi-chevron-left"), }));
({} as { VBtn: typeof __VLS_19; }).VBtn;
({} as { VBtn: typeof __VLS_19; }).VBtn;
const __VLS_21 = __VLS_20({ ...{ onClick: {} as any, }, variant: ("text"), icon: ("mdi-chevron-left"), }, ...__VLS_functionalComponentArgsRest(__VLS_20));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_19, typeof __VLS_21> & Record<string, unknown>) => void)({ ...{ onClick: {} as any, }, variant: ("text"), icon: ("mdi-chevron-left"), });
const __VLS_22 = __VLS_pickFunctionalComponentCtx(__VLS_19, __VLS_21)!;
let __VLS_23!: __VLS_NormalizeEmits<typeof __VLS_22.emit>;
let __VLS_24 = { 'click': __VLS_pickEvent(__VLS_23['click'], ({} as __VLS_FunctionalComponentProps<typeof __VLS_20, typeof __VLS_21>).onClick) };
__VLS_24 = {
click: $event => {
__VLS_ctx.rail = !__VLS_ctx.rail;
// @ts-ignore
[rail, rail,];
}
};
}
}
}
}
{
const __VLS_25 = ({} as 'VDivider' extends keyof typeof __VLS_ctx ? { 'VDivider': typeof __VLS_ctx.VDivider; } : 'vDivider' extends keyof typeof __VLS_ctx ? { 'VDivider': typeof __VLS_ctx.vDivider; } : 'v-divider' extends keyof typeof __VLS_ctx ? { 'VDivider': (typeof __VLS_ctx)["v-divider"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VDivider;
const __VLS_26 = __VLS_asFunctionalComponent(__VLS_25, new __VLS_25({ ...{}, }));
({} as { VDivider: typeof __VLS_25; }).VDivider;
({} as { VDivider: typeof __VLS_25; }).VDivider;
const __VLS_27 = __VLS_26({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_26));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_25, typeof __VLS_27> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_28 = __VLS_pickFunctionalComponentCtx(__VLS_25, __VLS_27)!;
let __VLS_29!: __VLS_NormalizeEmits<typeof __VLS_28.emit>;
}
{
const __VLS_30 = ({} as 'VList' extends keyof typeof __VLS_ctx ? { 'VList': typeof __VLS_ctx.VList; } : 'vList' extends keyof typeof __VLS_ctx ? { 'VList': typeof __VLS_ctx.vList; } : 'v-list' extends keyof typeof __VLS_ctx ? { 'VList': (typeof __VLS_ctx)["v-list"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VList;
const __VLS_31 = __VLS_asFunctionalComponent(__VLS_30, new __VLS_30({ ...{}, density: ("compact"), nav: (true), }));
({} as { VList: typeof __VLS_30; }).VList;
({} as { VList: typeof __VLS_30; }).VList;
const __VLS_32 = __VLS_31({ ...{}, density: ("compact"), nav: (true), }, ...__VLS_functionalComponentArgsRest(__VLS_31));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_30, typeof __VLS_32> & Record<string, unknown>) => void)({ ...{}, density: ("compact"), nav: (true), });
const __VLS_33 = __VLS_pickFunctionalComponentCtx(__VLS_30, __VLS_32)!;
let __VLS_34!: __VLS_NormalizeEmits<typeof __VLS_33.emit>;
{
const __VLS_35 = ({} as 'VListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.VListItem; } : 'vListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.vListItem; } : 'v-list-item' extends keyof typeof __VLS_ctx ? { 'VListItem': (typeof __VLS_ctx)["v-list-item"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VListItem;
const __VLS_36 = __VLS_asFunctionalComponent(__VLS_35, new __VLS_35({ ...{}, prependIcon: ("mdi-home-city"), title: ("Home"), value: ("home"), to: (({ name: 'home' })), }));
({} as { VListItem: typeof __VLS_35; }).VListItem;
({} as { VListItem: typeof __VLS_35; }).VListItem;
const __VLS_37 = __VLS_36({ ...{}, prependIcon: ("mdi-home-city"), title: ("Home"), value: ("home"), to: (({ name: 'home' })), }, ...__VLS_functionalComponentArgsRest(__VLS_36));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_35, typeof __VLS_37> & Record<string, unknown>) => void)({ ...{}, prependIcon: ("mdi-home-city"), title: ("Home"), value: ("home"), to: (({ name: 'home' })), });
const __VLS_38 = __VLS_pickFunctionalComponentCtx(__VLS_35, __VLS_37)!;
let __VLS_39!: __VLS_NormalizeEmits<typeof __VLS_38.emit>;
}
{
const __VLS_40 = ({} as 'VListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.VListItem; } : 'vListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.vListItem; } : 'v-list-item' extends keyof typeof __VLS_ctx ? { 'VListItem': (typeof __VLS_ctx)["v-list-item"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VListItem;
const __VLS_41 = __VLS_asFunctionalComponent(__VLS_40, new __VLS_40({ ...{}, prependIcon: ("mdi-account"), title: ("My Account"), value: ("account"), }));
({} as { VListItem: typeof __VLS_40; }).VListItem;
({} as { VListItem: typeof __VLS_40; }).VListItem;
const __VLS_42 = __VLS_41({ ...{}, prependIcon: ("mdi-account"), title: ("My Account"), value: ("account"), }, ...__VLS_functionalComponentArgsRest(__VLS_41));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_40, typeof __VLS_42> & Record<string, unknown>) => void)({ ...{}, prependIcon: ("mdi-account"), title: ("My Account"), value: ("account"), });
const __VLS_43 = __VLS_pickFunctionalComponentCtx(__VLS_40, __VLS_42)!;
let __VLS_44!: __VLS_NormalizeEmits<typeof __VLS_43.emit>;
}
{
const __VLS_45 = ({} as 'VListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.VListItem; } : 'vListItem' extends keyof typeof __VLS_ctx ? { 'VListItem': typeof __VLS_ctx.vListItem; } : 'v-list-item' extends keyof typeof __VLS_ctx ? { 'VListItem': (typeof __VLS_ctx)["v-list-item"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VListItem;
const __VLS_46 = __VLS_asFunctionalComponent(__VLS_45, new __VLS_45({ ...{}, prependIcon: ("mdi-information-variant-circle"), title: ("About"), value: ("about"), to: (({ path: '/about' })), }));
({} as { VListItem: typeof __VLS_45; }).VListItem;
({} as { VListItem: typeof __VLS_45; }).VListItem;
const __VLS_47 = __VLS_46({ ...{}, prependIcon: ("mdi-information-variant-circle"), title: ("About"), value: ("about"), to: (({ path: '/about' })), }, ...__VLS_functionalComponentArgsRest(__VLS_46));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_45, typeof __VLS_47> & Record<string, unknown>) => void)({ ...{}, prependIcon: ("mdi-information-variant-circle"), title: ("About"), value: ("about"), to: (({ path: '/about' })), });
const __VLS_48 = __VLS_pickFunctionalComponentCtx(__VLS_45, __VLS_47)!;
let __VLS_49!: __VLS_NormalizeEmits<typeof __VLS_48.emit>;
}
(__VLS_33.slots!).default;
}
{
const __VLS_50 = ({} as 'VMain' extends keyof typeof __VLS_ctx ? { 'VMain': typeof __VLS_ctx.VMain; } : 'vMain' extends keyof typeof __VLS_ctx ? { 'VMain': typeof __VLS_ctx.vMain; } : 'v-main' extends keyof typeof __VLS_ctx ? { 'VMain': (typeof __VLS_ctx)["v-main"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).VMain;
const __VLS_51 = __VLS_asFunctionalComponent(__VLS_50, new __VLS_50({ ...{}, class: ("d-flex align-center justify-center"), style: ({}), }));
({} as { VMain: typeof __VLS_50; }).VMain;
({} as { VMain: typeof __VLS_50; }).VMain;
const __VLS_52 = __VLS_51({ ...{}, class: ("d-flex align-center justify-center"), style: ({}), }, ...__VLS_functionalComponentArgsRest(__VLS_51));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_50, typeof __VLS_52> & Record<string, unknown>) => void)({ ...{}, class: ("d-flex align-center justify-center"), style: ({}), });
const __VLS_53 = __VLS_pickFunctionalComponentCtx(__VLS_50, __VLS_52)!;
let __VLS_54!: __VLS_NormalizeEmits<typeof __VLS_53.emit>;
{
const __VLS_55 = ({} as 'RouterView' extends keyof typeof __VLS_ctx ? { 'RouterView': typeof __VLS_ctx.RouterView; } : 'routerView' extends keyof typeof __VLS_ctx ? { 'RouterView': typeof __VLS_ctx.routerView; } : 'router-view' extends keyof typeof __VLS_ctx ? { 'RouterView': (typeof __VLS_ctx)["router-view"]; } : typeof __VLS_resolvedLocalAndGlobalComponents).RouterView;
const __VLS_56 = __VLS_asFunctionalComponent(__VLS_55, new __VLS_55({ ...{}, }));
({} as { RouterView: typeof __VLS_55; }).RouterView;
({} as { RouterView: typeof __VLS_55; }).RouterView;
const __VLS_57 = __VLS_56({ ...{}, }, ...__VLS_functionalComponentArgsRest(__VLS_56));
({} as (props: __VLS_FunctionalComponentProps<typeof __VLS_55, typeof __VLS_57> & Record<string, unknown>) => void)({ ...{}, });
const __VLS_58 = __VLS_pickFunctionalComponentCtx(__VLS_55, __VLS_57)!;
let __VLS_59!: __VLS_NormalizeEmits<typeof __VLS_58.emit>;
}
(__VLS_53.slots!).default;
}
(__VLS_8.slots!).default;
}
(__VLS_3.slots!).default;
}
if (typeof __VLS_styleScopedClasses === 'object' && !Array.isArray(__VLS_styleScopedClasses)) {
__VLS_styleScopedClasses["rounded"];
__VLS_styleScopedClasses["rounded-md"];
__VLS_styleScopedClasses["d-flex"];
__VLS_styleScopedClasses["align-center"];
__VLS_styleScopedClasses["justify-center"];
}
var __VLS_slots!: {};
return __VLS_slots;
}
