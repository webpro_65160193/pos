import type { Product } from "./Product";

export type ReceiptItem = {
    id: number;
    name: string;
    price: number;
    unit: number;
    productId: number;
    product?: Product;
  };