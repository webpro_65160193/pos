export type Product = {
  id: number;
  name: string;
  price: number;
  type: number;
};