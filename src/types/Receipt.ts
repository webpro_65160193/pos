import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";
import type { Member } from "./Member";

type Receipt =  {
    id: number;
    createdDate: Date;
    totalBefore: number;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    member?: Member;
    memberId: number;
    memberDiscount: number;
    receiptItems?: ReceiptItem[]
}

export type { Receipt }
