type Gender = 'male' | 'female' | 'other'
type Role = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullname: string
  gender: string // Male , Female, Others
  roles: string[] //admin , user
}

export type { Gender, Role, User }